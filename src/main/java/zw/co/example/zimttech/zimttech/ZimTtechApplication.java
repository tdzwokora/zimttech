package zw.co.example.zimttech.zimttech;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZimTtechApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZimTtechApplication.class, args);
    }

}
