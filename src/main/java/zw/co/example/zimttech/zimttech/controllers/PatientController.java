package zw.co.example.zimttech.zimttech.controllers;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import zw.co.example.zimttech.zimttech.models.Patient;
import zw.co.example.zimttech.zimttech.services.interfaces.IPatientService;

import java.util.List;

/**
 * @author Tinashe Dzwokora , on Thursday 11 May 2023 - 10:50
 * @project Zim-TTECH
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/patient")
@OpenAPIDefinition(info = @Info(title = "Zim-TTECH Patient API", version = "1.0", description = "Zim-TTECH Patient API v1.0"))
public class PatientController {
    private final IPatientService patientService;

    @PostMapping
    public ResponseEntity<Patient> createPatient(@RequestBody Patient patient) {
        return new ResponseEntity<>(patientService.createPatient(patient), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Patient> getPatientById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(patientService.getPatientById(id), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<Patient> updatePatient(@RequestBody Patient patient) {
        return new ResponseEntity<>(patientService.updatePatient(patient), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Patient> deletePatient(@PathVariable("id") Long id) {
        return new ResponseEntity<>(patientService.deletePatient(id),HttpStatus.ACCEPTED);
    }

    // Get all patients with Vital Signs with blood pressure greater than given value
    @GetMapping("/vital-signs/blood-pressure/{value}")
    public ResponseEntity<List<Patient>> getPatientsWithBloodPressureGreaterThan(@PathVariable("value") Double value){
        return new ResponseEntity<>(patientService.getPatientsWithBloodPressureGreaterThan(value), HttpStatus.OK);
    }
}
