package zw.co.example.zimttech.zimttech.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import zw.co.example.zimttech.zimttech.models.VitalSigns;
import zw.co.example.zimttech.zimttech.services.interfaces.IVitalSignsService;

import java.util.List;

/**
 * @author Tinashe Dzwokora , on Thursday 11 May 2023 - 12:30
 * @project Zim-TTECH
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/vital-signs")
public class VitalSignsController {
    private final IVitalSignsService vitalSignsService;
    @PostMapping
    public ResponseEntity<VitalSigns> createVitalSigns(@RequestBody VitalSigns vitalSigns){
        return new ResponseEntity<>(vitalSignsService.createVitalSigns(vitalSigns), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<VitalSigns> getVitalSignsById(@PathVariable("id") Long id){
        return new ResponseEntity<>(vitalSignsService.getVitalSignsById(id), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<VitalSigns> updateVitalSigns(@RequestBody VitalSigns vitalSigns){
        return new ResponseEntity<>(vitalSignsService.updateVitalSigns(vitalSigns), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<VitalSigns> deleteVitalSigns(@PathVariable("id") Long id){
        return new ResponseEntity<>(vitalSignsService.deleteVitalSigns(id), HttpStatus.ACCEPTED);
    }

    // Get all vital signs for a patient by patient id and date range
    @GetMapping("/patient/{id}")
    public ResponseEntity<List<VitalSigns>> getVitalSignsByPatientId(@PathVariable("id") Long id, @RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate){
        return new ResponseEntity<>(vitalSignsService.getVitalSignsByPatientId(id, startDate, endDate), HttpStatus.OK);
    }



}
