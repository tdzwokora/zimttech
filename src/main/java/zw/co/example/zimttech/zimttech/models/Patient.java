package zw.co.example.zimttech.zimttech.models;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

/**
 * @author Tinashe Dzwokora , on Thursday 11 May 2023 - 10:07
 * @project Zim-TTECH
 */
@Data
@Entity
@Table(name = "patient")
public class Patient extends BaseEntity {
    private String name;
    private int age;
    private String gender;
    @OneToMany(mappedBy = "patient")
    private List<VitalSigns> vitalSignsList;
}