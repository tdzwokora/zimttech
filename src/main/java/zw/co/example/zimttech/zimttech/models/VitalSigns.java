package zw.co.example.zimttech.zimttech.models;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author Tinashe Dzwokora , on Thursday 11 May 2023 - 10:09
 * @project Zim-TTECH
 */
@Data
@Entity
@Table(name = "vital_signs")
public class VitalSigns extends BaseEntity{
    private LocalDateTime dateTime;
    private double bloodPressure;
    private double weight;
    private double height;
    private double bloodGlucoseLevel;
    @ManyToOne
    @JoinColumn(name = "patient_id")
    private Patient patient;
}
