package zw.co.example.zimttech.zimttech.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import zw.co.example.zimttech.zimttech.models.Patient;

import java.util.List;

/**
 * @author Tinashe Dzwokora , on Thursday 11 May 2023 - 10:37
 * @project Zim-TTECH
 */
public interface PatientRepository extends JpaRepository<Patient,Long> {
    //Get all patients with Vital Signs with blood pressure greater than given value
    List<Patient> findByVitalSignsListBloodPressureGreaterThan(Double bloodPressureValue);
}
