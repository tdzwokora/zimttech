package zw.co.example.zimttech.zimttech.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import zw.co.example.zimttech.zimttech.models.Patient;
import zw.co.example.zimttech.zimttech.models.VitalSigns;

import java.util.List;

/**
 * @author Tinashe Dzwokora , on Thursday 11 May 2023 - 10:37
 * @project Zim-TTECH
 */
public interface VitalSignsRepository extends JpaRepository<VitalSigns,Long> {
    List<VitalSigns> findAllByPatientIdAndCreatedAtBetween(Long id, String startDate, String endDate);
}
