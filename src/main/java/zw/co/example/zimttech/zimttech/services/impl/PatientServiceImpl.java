package zw.co.example.zimttech.zimttech.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import zw.co.example.zimttech.zimttech.models.Patient;
import zw.co.example.zimttech.zimttech.repositories.PatientRepository;
import zw.co.example.zimttech.zimttech.services.interfaces.IPatientService;

import java.util.List;

/**
 * @author Tinashe Dzwokora , on Thursday 11 May 2023 - 10:42
 * @project Zim-TTECH
 */
@Service
@RequiredArgsConstructor
public class PatientServiceImpl implements IPatientService {
    private final PatientRepository patientRepository;
    @Override
    public Patient createPatient(Patient patient) {
        return patientRepository.save(patient) ;
    }

    @Override
    public Patient updatePatient(Patient patient) {
        return patientRepository.save(patient);
    }

    @Override
    public Patient getPatientById(Long id) {
        return patientRepository.findById(id).orElseThrow(()->new RuntimeException("Patient not found with id: "+id));
    }

    @Override
    public Patient deletePatient(Long id) {
        Patient patient = patientRepository.findById(id).orElseThrow(()->new RuntimeException("Patient not found with id: "+id));
        patient.setDeleted(true);
        return patientRepository.save(patient);
    }

    @Override
    public List<Patient> getPatientsWithBloodPressureGreaterThan(Double value) {
        return patientRepository.findByVitalSignsListBloodPressureGreaterThan(value);
    }


}
