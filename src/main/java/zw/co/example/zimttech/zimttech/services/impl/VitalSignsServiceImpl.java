package zw.co.example.zimttech.zimttech.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import zw.co.example.zimttech.zimttech.models.Patient;
import zw.co.example.zimttech.zimttech.models.VitalSigns;
import zw.co.example.zimttech.zimttech.repositories.VitalSignsRepository;
import zw.co.example.zimttech.zimttech.services.interfaces.IPatientService;
import zw.co.example.zimttech.zimttech.services.interfaces.IVitalSignsService;

import java.util.List;

/**
 * @author Tinashe Dzwokora , on Thursday 11 May 2023 - 10:42
 * @project Zim-TTECH
 */
@Service
@RequiredArgsConstructor
public class VitalSignsServiceImpl implements IVitalSignsService {
    private final VitalSignsRepository vitalSignsRepository;
    private final IPatientService patientService;
    @Override
    public VitalSigns createVitalSigns(VitalSigns vitalSigns) {
        Patient patient = patientService.getPatientById(vitalSigns.getPatient().getId());
        vitalSigns.setPatient(patient);
        return vitalSignsRepository.save(vitalSigns);
    }

    @Override
    public VitalSigns getVitalSignsById(Long id) {
        return vitalSignsRepository.findById(id).orElseThrow(()->new RuntimeException("Vital Signs not found with id: "+id));
    }

    @Override
    public VitalSigns updateVitalSigns(VitalSigns vitalSigns) {
        return vitalSignsRepository.save(vitalSigns);
    }

    @Override
    public VitalSigns deleteVitalSigns(Long id) {
        VitalSigns vitalSigns = vitalSignsRepository.findById(id).orElseThrow(()->new RuntimeException("Vital Signs not found with id: "+id));
        vitalSigns.setDeleted(true);
        return vitalSignsRepository.save(vitalSigns);
    }

    @Override
    public List<VitalSigns> getVitalSignsByPatientId(Long id, String startDate, String endDate) {
        return vitalSignsRepository.findAllByPatientIdAndCreatedAtBetween(id, startDate, endDate);
    }
}
