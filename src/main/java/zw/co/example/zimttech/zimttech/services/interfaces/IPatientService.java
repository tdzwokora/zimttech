package zw.co.example.zimttech.zimttech.services.interfaces;

import zw.co.example.zimttech.zimttech.models.Patient;

import java.net.URI;
import java.util.List;

/**
 * @author Tinashe Dzwokora , on Thursday 11 May 2023 - 10:40
 * @project Zim-TTECH
 */
public interface IPatientService {
    Patient createPatient(Patient patient);

    Patient updatePatient(Patient patient);

    Patient getPatientById(Long id);

    Patient deletePatient(Long id);

    List<Patient> getPatientsWithBloodPressureGreaterThan(Double value);
}
