package zw.co.example.zimttech.zimttech.services.interfaces;

import zw.co.example.zimttech.zimttech.models.VitalSigns;

import java.util.List;

/**
 * @author Tinashe Dzwokora , on Thursday 11 May 2023 - 10:40
 * @project Zim-TTECH
 */
public interface IVitalSignsService {
    VitalSigns createVitalSigns(VitalSigns vitalSigns);

    VitalSigns getVitalSignsById(Long id);

    VitalSigns updateVitalSigns(VitalSigns vitalSigns);

    VitalSigns deleteVitalSigns(Long id);

    List<VitalSigns> getVitalSignsByPatientId(Long id, String startDate, String endDate);
}
